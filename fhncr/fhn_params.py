'''

This is a convenience code to make the different parameter sets for Karma callable.
Options are:
  thesis
  A file from Auto bifurcation diagram (requires an index to select from within file, default 1)

'''

import numpy as np
from dedalus import public as de

def gendomain(N,L,dealias,complex=False):
   z_basis= de.Chebyshev('z', N, interval=(-L/2,+L/2), dealias=dealias)
   if complex:
      domain = de.Domain([z_basis], np.complex128)
   else:
      domain = de.Domain([z_basis], np.float64)
   z = domain.grid(0, scales=dealias)
   return (z_basis, domain, z)

def fhn_params(setname,index=1):
  
  p = np.loadtxt(setname)
  p = p[index-1,:]
  par = dict(alpha = p[1], beta = p[2], gamma = p[3], c = p[10], L = p[11], J = p[12], D1 = p[15], D2 = p[16])
  
  return par

