import numpy as np
from scipy import interpolate

def load_and_interp_auto_sol(filename,xx):
    sol = np.loadtxt(filename)
    x,y = sol[:,0], sol[:,1:-1]
    f = interpolate.PchipInterpolator(x, y, axis=0) 
    #f = interp1d(x, y, axis=0) #, kind=3) # leads to huge memory usage
    yy = f(xx)

    u = []
    for n in range(yy.shape[1]):
        u.append(yy[:,n])

    return np.array(u)

def dilate_and_scale_sol(u, amp):
    U = np.copy(u)
    U[0] = U[0]*amp
    return U
    
def tanhpert(x,u,extent=1.0,strength=0.0):
    U = np.copy(u)
    #pert = strength*(1.0 - np.tanh(100.0*(x-extent)))/2.0
    pert = strength*(1.0-np.sign(x-extent))/2.0
    U[0] += pert
    return U
    
def gaussianpert(x,u,extent,strength):
    U = np.copy(u)
    pert = strength*np.exp(-(x/extent)**2.0)
    U[0] += pert
    return U
