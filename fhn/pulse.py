"""
Usage:
    pulse.py [options]

Options:
   --soldir=<soldir>             solution directory                
"""

from docopt import docopt
import h5py

import numpy as np
import matplotlib.pyplot as plt

from dedalus import public as de

import pathlib

import logging
logger = logging.getLogger(__name__)

import pert

args = docopt(__doc__)

soldir = args['--soldir']

# load the BVP solution and parameters
BVP = h5py.File(f'{soldir}/BVP.h5', 'r')

# params
p = {}
for pp in ['alpha','beta','gamma','c','L']:
	p[pp] = np.ravel(BVP[pp])[0]

# define FHN-q nonlinearity parameter
try:
	p['q'] = np.ravel(BVP['q'])[0]
except:
	p['q'] = 1.0
	pass
	
# discretization
N = int(np.ravel(BVP['N'])[0])
dealias = np.ravel(BVP['dealias'])[0]

# critical wave and critical wave grid translated to [0,L]
slow = [ np.ravel(BVP['u']) , np.ravel(BVP['v']) ]
z0 = np.ravel(BVP['z'])
z0 = z0 + (z0[0]<0.0)*(p['L']/2)

# change N <- N*dealias, dealias <- 1
N = int(N*dealias)
dealias = 1

# potentially change domain size for DNS
p['L'] = 300.0

# define domain
z_basis= de.Chebyshev('z', N, interval=(0,p['L']), dealias=dealias)
domain = de.Domain([z_basis], np.float64)
z = domain.grid(0, scales=dealias)

T = 2.0**(1.0+np.min([np.floor(np.log2(p['L']/p['c'])),23.0]))
dt= 2.0**-3

u0 =[0.0,0.0]

rest = [ u0[0]*np.ones(N*dealias,) , u0[1]*np.ones(N*dealias,) ]

wave = pert.tanhpert(z,rest,extent=1.0,strength=1.0)

###
# run evolution to find stable pulse shape 
#	f(t) = [ u(t,x=x0) , v(t,x=x0) , uz(t,x=x0) ]
###

# Problem
problem = de.IVP(domain, variables=['u', 'uz', 'v'])

problem.meta[:]['z']['dirichlet'] = True

for par in p.keys():
      problem.parameters[par] = p[par]

problem.substitutions['f1(u,v)'] = "u*(u-beta)*(1-u) - v**q"
problem.substitutions['f2(u,v)'] = "gamma*(alpha*u - v)"

problem.add_equation("dt(u) - dz(uz) = f1(u,v)")
problem.add_equation("dz(u) - uz = 0")
problem.add_equation("dt(v) = f2(u,v)")

problem.add_bc("left(uz)  = 0")
problem.add_bc("right(uz) = 0")

# Build solver
solver = problem.build_solver(de.timesteppers.SBDF4)
solver.stop_sim_time  = T
solver.stop_wall_time = np.inf
solver.stop_iteration = np.inf

# Initial conditions
u = solver.state['u']
v = solver.state['v']
uz= solver.state['uz']
 
u.set_scales(dealias, keep_data=True)
v.set_scales(dealias, keep_data=True)
uz.set_scales(dealias, keep_data=True)

# assign initial conditions
u['g'] = np.copy(wave[0])
v['g'] = np.copy(wave[1])
u.differentiate(0, out=uz)

u.set_scales(dealias, keep_data=True)
v.set_scales(dealias, keep_data=True)
uz.set_scales(dealias, keep_data=True)

# define directory
pulsedir = f'{soldir}/pulse/'

# path
pathlib.Path(f'{pulsedir}').mkdir(parents=True, exist_ok=True)

# evolution stuff
analysis = solver.evaluator.add_file_handler(pulsedir, sim_dt=1.0, max_writes=np.inf, mode='overwrite')
#analysis.add_task("u", name='u')
#analysis.add_task("v", name='v')
#analysis.add_task("uz", name='uz')
analysis.add_task("integ(abs(u-{0}),'z')".format(u0[0]), name='psi')
analysis.add_task("interp(u,z={0})".format(p['L']/2), name='u(t)')
analysis.add_task("interp(v,z={0})".format(p['L']/2), name='v(t)')
analysis.add_task("interp(uz,z={0})".format(p['L']/2),name='uz(t)')

# Main loop
Ccheck = True

logger.info('t = {0:4.1f}, t/T = {1:2.16f}'.format(solver.sim_time,solver.sim_time/T))
while solver.ok:
	
	solver.step(dt)
	
	if solver.iteration % int(1/dt) == 0:
		logger.info('t = {0:4.1f}, t/T = {1:2.16f}'.format(solver.sim_time,solver.sim_time/T))
		
		if np.any(np.isnan(u['g'])):
			break
		
		if np.sqrt( (u['g'][-1]-u0[0])**2 + (v['g'][-1]-u0[1])**2 + (uz['g'][-1])**2 ) > 1e-3 and Ccheck == True:
			Tpulse = solver.sim_time
			print(f'Pulse time t={Tpulse:2.16f}')
			
			pulse = [ np.copy(u['g']) , np.copy(v['g']) ]
			Ccheck = False
			
			# plot U
			plt.figure()
			plt.plot(z0, slow[0], label=r"$\hat{u}_1(x)$")
			plt.plot(z0, slow[1], label=r"$\hat{u}_2(x)$")
			plt.plot(z, pulse[0], label=r"$\check{u}_1(x)$")
			plt.plot(z, pulse[1], label=r"$\check{u}_2(x)$")
			plt.xlabel(r"$\xi$")
			plt.legend(loc=0,edgecolor=(1,1,1),facecolor=(1,1,1),framealpha=0.9)
			plt.gcf().set_size_inches(1.618*3, 3)
			plt.savefig(f'{pulsedir}/U.svg',bbox_inches='tight')
			plt.close()
		
		if np.sqrt( (u['g'][-1]-u0[0])**2 + (v['g'][-1]-u0[1])**2 + (uz['g'][-1])**2 ) < 1e-3 and Ccheck == False:
			break
			
with h5py.File(f'{pulsedir}/pulse_s1/pulse_s1_p0.h5', mode='r') as file:
   t 	= np.ravel(file['scales']['sim_time'])
   psi 	= np.ravel(file['tasks']['psi'])
   ut 	= np.ravel(file['tasks']['u(t)'])
   vt 	= np.ravel(file['tasks']['v(t)'])
   uzt	= np.ravel(file['tasks']['uz(t)'])
   
plt.figure()
plt.plot(t, psi, label=r"$\psi(t)$")
plt.xlabel(r"$t$")
plt.legend(loc=0,edgecolor=(1,1,1),facecolor=(1,1,1),framealpha=0.9)
plt.gcf().set_size_inches(1.618*3, 3)
plt.savefig(f'{pulsedir}/psi.svg',bbox_inches='tight')
plt.close()

# try to compute c
U = np.copy(pulse[0])
Uz = np.copy(pulse[0])
Uzz = np.copy(pulse[0])
V = np.copy(pulse[1])
Vz = np.copy(pulse[1])

q = domain.new_field(name='q')
q.set_scales(dealias)
qz = domain.new_field(name='qz')
qz.set_scales(dealias)

q['g'] = np.copy(pulse[1])
q.differentiate('z',out=qz)
Vz=np.copy(qz['g'])

q['g'] = np.copy(pulse[0])
q.differentiate('z',out=qz)
Uz=np.copy(qz['g'])

q['g'] = np.copy(qz['g'])
q.differentiate('z',out=qz)
Uzz=np.copy(qz['g'])

f1 = U*(1.0-U)*(U-p['beta']) - V**p['q']
f2 = p['gamma']*(p['alpha']*pulse[0]-pulse[1])

ff = lambda c: np.sum((Uzz + c*Uz + f1)**2 + (c*Vz + f2)**2)

from scipy.optimize import minimize
res = minimize(ff, p['L']/Tpulse)
print(res)
print(f'C={res.x}')

# save speed (approximate) to file
np.savetxt(f'{pulsedir}/fastC',np.array([res.x]).T,fmt='%+2.16f')

# save data to text file
np.savetxt(f'{pulsedir}/pulse',np.array([z,pulse[0],pulse[1]]).T,fmt='%+2.16f')

# save data to text file
np.savetxt(f'{pulsedir}/fastwave',np.array([t,ut,vt,uzt]).T,fmt='%+2.16f %+2.16f %+2.16f %+2.16f')

plt.figure()
plt.plot(slow[0],slow[1],'-',label=r'$\hat{\mathbf{u}}$')
plt.plot(ut,vt,'-',label=r'$\check{\mathbf{u}}$')
plt.plot(u0[0],u0[1],'.k',label=r'$\bar{\mathbf{u}}$')
plt.xlabel(r"$u_1$")
plt.ylabel(r"$u_2$")
plt.legend(loc=0,edgecolor=(1,1,1),facecolor=(1,1,1),framealpha=0.9)
plt.savefig(f'{pulsedir}/uv.svg',bbox_inches='tight')
plt.close()
