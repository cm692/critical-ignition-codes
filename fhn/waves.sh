#!/bin/bash

for N in 512; do

	mkdir N_$(($N))

	for L in 150; do

		soldir=N_$(($N))/L_$(($L))/
		mkdir $soldir

		echo "N=$N and L=$L, base directory: $soldir"

		python3 waves.py --N=$N --L=$L --soldir=$soldir
		
		for j in {0..10}; do
			python3 sweep.py --soldir=$soldir/$((j))/
		done
		
	done
done
